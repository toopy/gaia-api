Gaia-API
========

Gaia-API centralizes data and logic of the `Project Gaia`. It allows updates
and retrievals of these values with handy JSON API.

First it collects `uptakes` from several `probes` installed to measure
energy consumption.

It computes the corresponding carbon dioxyde according `categories` of
collected values.

And it tries to match configured `prospective` with these results.


Install
-------

As python project `gaia-api` is pip installed as follow after cloning the git
repository until it has been released for real:

.. code:: bash

    (env)$ pip install -e .


Configuration
-------------

Gaia-API is based on `starlette`. More info about the configuration format can
be found here: https://www.starlette.io/config/.

Available variables:

.. code:: bash

    DATABASE_URL=postgresql://127.0.0.1:5432/gaia
    DEBUG=False
    HOST=127.0.0.1
    PORT=8000


Database
--------

The database schema can be setup as follow:

.. code:: bash

    (env)$ createdb gaia
    (env)$ gaia-cli initdb


Run the Server
--------------

The API server can be started as follow:

.. code:: bash

    (env)$ gaia-api
    ...
    INFO: Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)


Run the Prospective Matching Command
------------------------------------

The update command can be started as follow:

.. code:: bash

    (env)$ gaia-cli update

*Note:* This command should replaced by a dedicated worker that triggers the update periodically.


The API
-------

Add a Category
^^^^^^^^^^^^^^

.. code:: bash

    $ http --json POST 127.0.0.1:8000/categories \
    >     name="energy" \
    >     parent:=null
    HTTP/1.1 201 OK
    content-length: 22
    content-type: application/json
    date: Wed, 22 May 2019 18:55:19 GMT
    server: uvicorn

    {
        "id": 1,
        "name": "energy",
        "parent": null
    }


Listing Categories
^^^^^^^^^^^^^^^^^^

.. code:: bash

    $ http GET 127.0.0.1:8000/categories
    HTTP/1.1 200 OK
    content-type: application/json
    date: Wed, 22 May 2019 18:55:08 GMT
    server: uvicorn
    transfer-encoding: chunked

    [
        {
            "id": 1,
            "name": "energy",
            "parent": null
        }
    ]


Add an Uptake
^^^^^^^^^^^^^

.. code:: bash

    $ http --json POST 127.0.0.1:8000/uptakes \
    >     category:=1 \
    >     unit="uW" \
    >     source:=3ee60e4c-35db-4fc8-acfa-e572c493860d \
    >     value:=6996000
    HTTP/1.1 201 OK
    content-length: 48
    content-type: application/json
    date: Wed, 22 May 2019 18:13:06 GMT
    server: uvicorn

    [
      {
        "id": 1,
        "category": 1,
        "date": "2019-05-22T18:13:06.845610",
        "source": "3ee60e4c-35db-4fc8-acfa-e572c493860d",
        "unit": "uW",
        "value": 6996000
      }
    ]


Listing Footprints
^^^^^^^^^^^^^^^^^^

.. code:: bash

    $ http GET 127.0.0.1:8000/footprints

    HTTP/1.1 200 OK
    content-type: application/json
    date: Wed, 22 May 2019 23:34:09 GMT
    server: uvicorn
    transfer-encoding: chunked

    [
        {
            "category": 1,
            "date": "2019-05-22T23:00:00",
            "id": 16,
            "value": 14
        }
    ]


Add a Prospective
^^^^^^^^^^^^^^^^^

.. code:: bash

    $ http --json POST 127.0.0.1:8000/prospectives \
    >     category:=1 \
    >     description="Take a break or increase this prospective threshold if you are performing the Ultimate 48hrs competition." \
    >     title="Less use of your laptop could help to save the climate" \
    >     matching_criteria:='{"threshold": 8}' \
    >     potential_gain:=null
    HTTP/1.1 201 OK
    content-length: 279
    content-type: application/json
    date: Wed, 22 May 2019 18:55:19 GMT
    server: uvicorn

    {
        "category": 1,
        "description": "Take a break or increase this prospective threshold if you are performing the Ultimate 48hrs competition.",
        "enabled": false,
        "id": 2,
        "matching_criteria": {
            "threshold": 8
        },
        "potential_gain": 0,
        "title": "Less use of your laptop could help to save the climate"
    }



Listing Prospectives
^^^^^^^^^^^^^^^^^^^^

.. code:: bash

    $ http GET 127.0.0.1:8000/prospectives?enabled=true
    HTTP/1.1 200 OK
    content-type: application/json
    date: Wed, 22 May 2019 23:35:57 GMT
    server: uvicorn
    transfer-encoding: chunked

    [
        {
            "category": 1,
            "description": "Take a break or increase this prospective threshold if you are performing the Ultimate 48hrs competition.",
            "enabled": true,
            "id": 2,
            "matching_criteria": {
                "threshold": 8
            },
            "potential_gain": 6,
            "title": "Less use of your laptop could help to save the climate"
        }
    ]


License
-------

MIT License
