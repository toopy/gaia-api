import asyncio
import uvicorn
import sys
from datetime import datetime
from datetime import timedelta

import click

from .app import app
from .calculators import EnergyCalculator
from .config import config
from .db import db
from .matchers import EnergyMatcher


def api():
    uvicorn.run(
        app,
        host=config('HOST', default='127.0.0.1'),
        port=config('PORT', cast=int, default=8000)
    )


@click.group()
def cli():
    pass


async def _initdb():
    await db.set_bind(config('DATABASE_URL'))
    from .models import Category  # noqa
    from .models import Footprint  # noqa
    from .models import Prospective  # noqa
    from .models import Uptake  # noqa
    await db.gino.create_all()
    await db.pop_bind().close()


@cli.command()
def initdb():
    asyncio.get_event_loop().run_until_complete(
        _initdb()
    )


async def _update():
    # TODO compute last range
    end_date = datetime.now().replace(minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(hours=1)

    await db.set_bind(config('DATABASE_URL'))
    from .models import Category
    from .models import Uptake
    from .models import Footprint
    from .models import Prospective

    async with db.transaction():
        # iter categories
        category_query = Category.query
        async for category in category_query.gino.iterate():
            footprint_query = Footprint.query.where(
                Footprint.category == category.id)
            footprint_query = footprint_query.where(
                Footprint.date == end_date
            )
            #
            footprint = await footprint_query.gino.first()
            # uptodate
            if footprint:
                continue
            # iter uptakes from the last range
            uptake_query = Uptake.query.where(Uptake.category == category.id)
            uptake_query = uptake_query.where(start_date < Uptake.date)
            uptake_query = uptake_query.where(Uptake.date <= end_date)
            # we assume all the uptakes as the same unit (uw for the demo)
            unit = 'uw'
            total = 0
            async for uptake in uptake_query.gino.iterate():
                total += uptake.value
            # TODO add a constraint on columns (date, category)
            # compute footprint
            footprint = await Footprint.create(
                category=category.id,
                date=end_date,
                value=EnergyCalculator(unit, total).footprint,
            )
            # compute prospective
            prospective_query = Prospective.query.where(
                Prospective.category == category.id)
            matcher = None
            async for prospective in prospective_query.gino.iterate():
                matcher = EnergyMatcher(
                    footprint.value,
                    **(prospective.matching_criteria or {})
                )
                # enable the prospective
                await prospective.update(
                    enabled=matcher.match,
                    potential_gain=matcher.potential_gain
                ).apply()
            sys.stdout.write('[{}] {}: {} -> {}\n'.format(
                end_date,
                category.name,
                footprint.value,
                'match' if (matcher and matcher.match) else 'not match'
            ))


@cli.command()
def update():
    asyncio.get_event_loop().run_until_complete(
        _update()
    )
