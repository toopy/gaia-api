

class EnergyCalculator:

    def __init__(self, unit, value):
        self.unit = unit
        self.value = value

    @property
    def footprint(self):
        # with uw as unit should return watts
        return int(self.value / 1000000000.)
