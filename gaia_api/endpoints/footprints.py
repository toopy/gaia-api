import json

from starlette.responses import StreamingResponse

from ..app import app
from ..db import db
from ..models import Footprint


async def _list_footprints():
    first = True
    yield '['
    async with db.transaction():
        async for footprint in Footprint.query.gino.iterate():
            if not first:
                yield ','
            first = False
            yield json.dumps({
                'id': footprint.id,
                'category': footprint.category,
                'date': footprint.date.isoformat(),
                'value': footprint.value,
            })
    yield ']'


@app.route('/footprints', methods=['GET'])
async def list_footprints(request):
    return StreamingResponse(
        _list_footprints(),
        media_type='application/json'
    )
