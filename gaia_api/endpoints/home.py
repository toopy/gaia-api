from starlette.responses import JSONResponse

from ..app import app


@app.route('/')
async def home(request):
    return JSONResponse({'project': 'gaia'})
