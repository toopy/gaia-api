import json

from starlette.responses import JSONResponse
from starlette.responses import StreamingResponse

from ..app import app
from ..db import db
from ..models import Category


@app.route('/categories', methods=['POST'])
async def add_category(request):
    data = await request.json()
    category = await Category.create(
        name=data['name'],
        parent=data['parent'],
    )
    return JSONResponse(
        {
            'id': category.id,
            'name': category.name,
            'parent': category.parent,
        },
        status_code=201,
    )


async def _list_categories():
    first = True
    yield '['
    async with db.transaction():
        async for category in Category.query.gino.iterate():
            if not first:
                yield ','
            first = False
            yield json.dumps({
                'id': category.id,
                'name': category.name,
                'parent': category.parent,
            })
    yield ']'


@app.route('/categories', methods=['GET'])
async def list_categories(request):
    return StreamingResponse(
        _list_categories(),
        media_type='application/json'
    )
