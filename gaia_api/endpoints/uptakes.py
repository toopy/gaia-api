import json

from starlette.responses import JSONResponse
from starlette.responses import StreamingResponse

from ..app import app
from ..db import db
from ..models import Uptake


@app.route('/uptakes', methods=['POST'])
async def add_uptake(request):
    data = await request.json()
    uptake = await Uptake.create(
        category=data['category'],
        unit=data['unit'],
        source=data['source'],
        value=data['value'],
    )
    return JSONResponse(
        {
            'id': uptake.id,
            'category': uptake.category,
            'date': uptake.date.isoformat(),
            'unit': uptake.unit,
            'source': str(uptake.source),
            'value': uptake.value,
        },
        status_code=201,
    )


async def _list_uptakes():
    first = True
    yield '['
    async with db.transaction():
        async for uptake in Uptake.query.gino.iterate():
            if not first:
                yield ','
            first = False
            yield json.dumps({
                'id': uptake.id,
                'category': uptake.category,
                'date': uptake.date.isoformat(),
                'source': str(uptake.source),
                'unit': uptake.unit,
                'value': uptake.value,
            })
    yield ']'


@app.route('/uptakes', methods=['GET'])
async def list_uptakes(request):
    return StreamingResponse(
        _list_uptakes(),
        media_type='application/json'
    )
