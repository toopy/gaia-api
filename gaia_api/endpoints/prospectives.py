import json
from functools import partial

from starlette.responses import JSONResponse
from starlette.responses import StreamingResponse

from ..app import app
from ..db import db
from ..models import Prospective


@app.route('/prospectives', methods=['POST'])
async def add_prospective(request):
    data = await request.json()
    prospective = await Prospective.create(
        category=data['category'],
        description=data['description'],
        title=data['title'],
        matching_criteria=data['matching_criteria'],
    )
    return JSONResponse(
        {
            'id': prospective.id,
            'category': prospective.category,
            'description': prospective.description,
            'enabled': prospective.enabled,
            'title': prospective.title,
            'matching_criteria': prospective.matching_criteria,
            'potential_gain': prospective.potential_gain,
        },
        status_code=201,
    )


async def _list_prospectives(filter_enabled=False, enabled=None):
    first = True
    yield '['
    async with db.transaction():
        prospective_query = Prospective.query
        if filter_enabled:
            prospective_query = prospective_query.where(
                Prospective.enabled == enabled
            )
        async for prospective in prospective_query.gino.iterate():
            if not first:
                yield ','
            first = False
            yield json.dumps({
                'id': prospective.id,
                'category': prospective.category,
                'description': prospective.description,
                'enabled': prospective.enabled,
                'title': prospective.title,
                'matching_criteria': prospective.matching_criteria,
                'potential_gain': prospective.potential_gain,
            })
    yield ']'


@app.route('/prospectives', methods=['GET'])
async def list_prospectives(request):
    enabled = request.query_params.get('enabled')
    if enabled:
        list_func = partial(
            _list_prospectives,
            filter_enabled=True,
            enabled=enabled in ('True', 'true', 'yes', '1')
        )
    else:
        list_func = _list_prospectives
    return StreamingResponse(
        list_func(),
        media_type='application/json'
    )
