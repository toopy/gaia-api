from datetime import datetime

from ..db import db


class Prospective(db.Model):
    __tablename__ = 'prospectives'

    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(None, db.ForeignKey('categories.id'))
    description = db.Column(db.Text, nullable=False)
    enabled = db.Column(db.Boolean, default=False)
    title = db.Column(db.String, nullable=False)
    matching_criteria = db.Column(db.JSON)
    potential_gain = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now,
                           onupdate=datetime.now)
