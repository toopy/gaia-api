from ..db import db


class Footprint(db.Model):
    __tablename__ = 'footprints'

    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(None, db.ForeignKey('categories.id'))
    date = db.Column(db.DateTime, nullable=False)
    value = db.Column(db.Integer, nullable=False)
