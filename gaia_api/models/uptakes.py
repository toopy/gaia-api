from datetime import datetime

from sqlalchemy.dialects.postgresql import UUID

from ..db import db


class Uptake(db.Model):
    __tablename__ = 'uptakes'

    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(None, db.ForeignKey('categories.id'))
    date = db.Column(db.DateTime, default=datetime.now)
    unit = db.Column(db.String, nullable=False)
    source = db.Column(UUID, nullable=False)
    value = db.Column(db.Integer, nullable=False)
