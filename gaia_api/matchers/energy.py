

class EnergyMatcher:

    def __init__(self, footprint, threshold=8, **kw):
        self.footprint = footprint
        self.threshold = threshold

    @property
    def match(self):
        return self.footprint > self.threshold

    @property
    def potential_gain(self):
        if not self.match:
            return 0
        else:
            return self.footprint - self.threshold
