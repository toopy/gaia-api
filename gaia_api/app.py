from json.decoder import JSONDecodeError

from asyncpg.exceptions import UniqueViolationError
from starlette.applications import Starlette
from starlette.responses import JSONResponse

from .config import config
from .db import db


app = Starlette(debug=config('DEBUG', cast=bool, default=False))


@app.middleware('http')
async def catch_json_errors(request, call_next):
    try:
        return await call_next(request)
    except (JSONDecodeError, KeyError):
        return JSONResponse(
            {'message': 'invalid payload'},
            status_code=422,
        )


@app.middleware('http')
async def catch_db_errors(request, call_next):
    try:
        return await call_next(request)
    except UniqueViolationError:
        return JSONResponse(
            {'message': 'already exist'},
            status_code=400,
        )


@app.on_event('startup')
async def startup():
    await db.set_bind(config('DATABASE_URL'))
    from .endpoints import home  # noqa


@app.on_event('shutdown')
async def shutdown():
    await db.pop_bind().close()
